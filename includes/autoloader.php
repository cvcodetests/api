<?php 

spl_autoload_register('myAutoLoader');

function myAutoLoader($class){
    $path = ROOTPATH . "/models/";
    $extension = ".class.php";
    $full_path = strtolower($path . $class . $extension);

    if(!file_exists($full_path)){
        return false;
    }

    include_once($full_path);
}

?>