Ocasta API Code Challenge
http://challenge.ocasta.com/
http://3.20.220.165/api/

Design choices...
----
- Everything is routed to the index.php page with .htaccess. This avoids having to use multiple files and allows the API to function as a single something.

- Index.php acts as the controller, receiving the API queries and pulling responses from the models which are output along with the appropriate response code. If I was to spend some more time on this I would look to move some of the contents, such as the response codes, to a seperate class.

- The class files handle the SQL and return some output to index.php where it is printed. If the methods are successful they respond with JSON, if not errors are caught and returned as a string back to the controller. From here, I simply check if it's JSON or string to know if it's successful or an error.

- The included config and autoloader I typically use every project (unless using composer).

- The database structure is pretty standard. One thing of note is I used the occupier's name rather than a boolean in the rooms table. By using the room's occupier/null, you effectively have a boolean but with more information. It could be said that this is redundant as the occupier's name is stored in the history table but there isn't any additional cost, queries don't require joins and if changes were made to the database it's in a more robust state. The API still displays available/unavailable as per the spec.


With additional time..
----
- More testing before release. Automated testing.
- Expand users class. 
- Stop users booking mutliple rooms.
- Ensure there is consistent style throughout.
- SSL


Set up 
----
- Use sql.sql to create database. Included is 2 users, one admin and one non-admin.
- Update config.php to match database settings


Notes
----
POST room requires both name and id where as spec requires just name.  

{
  "name": "Situation Room",
  "id": "def"
}

