<?php

Class Users {
	
	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function getUsers(){
		$sql = "SELECT id, user, admin FROM users";
		$users = [];
		foreach ($this->db->query($sql) as $row) {
			//mysql uses 0/1, change to false/true
		    $users[$row['user']] = (bool) $row['admin'];
		}
		if(sizeof($users) > 0){
			return $users;
		}	
		return false;
	}

	public function getUserByName($name){
		$sql = "SELECT id, user, admin FROM users WHERE user=:user";
		$sth = $this->db->prepare($sql);
		$sth->execute(array(':user' => $name));
		$res = $sth->fetch();
		return $res;
	}
}