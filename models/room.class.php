<?php

Class Room {

	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function getRooms(){
		$sql = 'SELECT `id`, `name`, IF(`user` is null ,true, false) as available FROM rooms ORDER BY id';
		$res = $this->db->query($sql);
		$rooms = [];
		if($res){
			foreach ($this->db->query($sql) as $row) {
				//mysql uses 0/1, change to false/true
				$row['available'] = (bool)$row['available']; 
			    $rooms[] = $row;
			}
		} else {
			return "No rooms found";
		}
		return json_encode($rooms);
	}

	public function getRoom($id){
		$sql = 'SELECT `id`, `name`, IF(user is null ,true, false) as available FROM rooms WHERE id = :id ORDER BY id';
		$sth = $this->db->prepare($sql);
		$sth->execute(array(':id' => $id));
		$res = $sth->fetch();
		if($res){
			$res['available'] = (bool)$res['available']; 
			return   json_encode($res);
		} else {
			return "Room not found.";
		}
	}

	public function getRoomByName($name){
		$sql = 'SELECT `id`, `name`, IF(`user` is null ,true, false) as available FROM `rooms` WHERE `name` = :name';
		$sth = $this->db->prepare($sql);
		$sth->execute(array(':name' => $name));
		$res = $sth->fetch();
		if($res){
			$res['available'] = (bool)$res['available']; 
			return   json_encode($res);
		} else {
			return "Room not found.";
		}
	}

	public function getRoomUsage($id, $start, $end){

		try {
			$start_datetime = new Datetime($start);
			$end_datetime = new Datetime($end);
		} catch (Exception $e) {
		    return "Invalid date format";
		}
		if($id == null){
			$sql = "SELECT `id`, `room_id`, `start_date`, `end_date`, IF(user is null ,true, false) as `available`   from `history` where `start_date` >= str_to_date(:start, '%Y-%m-%d %H:%i:%s') AND (`end_date` <= str_to_date(:end, '%Y-%m-%d %H:%i:%s') OR end_date is null)";
			$sth= $this->db->prepare($sql);
			$sth->execute(array(':start' => $start_datetime->format("Y-m-d H:i:s"), ':end' => $end_datetime->format("Y-m-d H:i:s")));
		} else {
			$sql = "SELECT `id`, `room_id`, `start_date`, `end_date`, IF(user is null ,true, false) as `available`  from `history` where `start_date` >= str_to_date(:start, '%Y-%m-%d %H:%i:%s') AND (`end_date` <= str_to_date(:end, '%Y-%m-%d %H:%i:%s') or end_date is null) AND `room_id` = :id";
			$sth = $this->db->prepare($sql);
			$sth->execute(array(':id' => $id, ':start' => $start_datetime->format("Y-m-d H:i:s"), ':end' => $end_datetime->format("Y-m-d H:i:s")));
		}
		$res = $sth->fetchAll();
		$rooms = [];
		if($res){
			foreach ($res as $row) {
				$start_datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $row['start_date']);
				$end_datetime = \DateTime::createFromFormat("Y-m-d H:i:s", $row['end_date']);
				$row['start_date'] = $start_datetime->format(\DateTime::RFC3339);
				if(isset($row['end_date'])){
					$row['end_date'] = $end_datetime->format(\DateTime::RFC3339);
				}
				
				//mysql uses 0/1, change to false/true
				$row['available'] = (bool)$row['available']; 
				$rooms[] = $row;
			}
		}
		return json_encode($rooms);
	}

	public function putRoomName($id,$name){
		$sql = "UPDATE `rooms` SET `name`=:name WHERE `id`=:id";
		if($this->getRoom($id)== "Room not found."){
			return "Room not found.";
		}
		if($this->getRoomByName($name)!= "Room not found."){
			return "Room name already in use.";
		}
		$this->db->prepare($sql)->execute(array(':id' => $id,':name' => $name));
		return $this->getRoom($id);
	}

	public function putRoomUser($id,$user){
		if($user == false){
			$user = null;
		}
		$decoded_room = json_decode($this->getRoom($id));
		if($decoded_room){
			$query = $this->db->prepare("SELECT `id`,`name`,`user` FROM `rooms` WHERE id = :id");
			$query->execute(array(':id' => $id));
			$row = $query->fetch();

			if($user!= null && $row['user'] != null){
				return "Occupied";
				//room is taken - show error
			} else if ($user != null && $row['user'] == null){
				//room is not taken make occupied 
				$sql = "UPDATE rooms SET user=:user WHERE id=:id";
				try {
					$this->db->prepare($sql)->execute(array(':id' => $id,':user' => $user));
				 
				} catch (Exception $e) {

					if ($e->errorInfo[1] == 1062) {
					      return "Room ID already in use.";
					} else {
						return "Server error";
					}
				}
				$sql = "INSERT INTO history (`room_id`, `start_date`, `user`) VALUES (:id, now() , :user)";
				$stmt= $this->db->prepare($sql)->execute(array(':id' => $id,':user' => $user));
				return $this->getRoom($id);

			} else if ($user == null && $row['user'] != null){
				//room is taken make vacant
				$sql = "UPDATE `rooms` SET `user`=:user WHERE id=:id";
				$this->db->prepare($sql)->execute(array(':id' => $id,':user' => $user));
				$sql = "UPDATE `history` SET `end_date`=NOW() WHERE `room_id`=:id AND `end_date` is null";
				$this->db->prepare($sql)->execute(array(':id' => $id));
				return $this->getRoom($id);
			} else {
				///room already vacant
				return "Room is already available";
			}
		} else {
			return "Room not found.";
		}
	}


	///Challenge spec requires only name and not id. Without having an initial id it is not possible to update the id later
	public function postRoom($id,$name){
		if($id == null || $id ==""){
			 return "Room ID required";
		}
		if($name == null || $name == ""){
			 return "Room name required";
		}

		$sql = "INSERT INTO rooms (`id`, `name`) VALUES (:id, :name)";
		$stmt= $this->db->prepare($sql);

		try {
		   $stmt->execute(array(':id' => $id,':name' => $name)); 
		} catch (Exception $e) {

			if ($e->errorInfo[1] == 1062) {
			      return "Room name or ID already in use.";
			} else {
				return "Server error";
				///check max size
			}
		}
		return $this->getRoom($id);	
	}

	public function responseGenerator($data, $responses = null){

		$decoded = json_decode($data);
		///Check if response is error or JSON
		if((json_last_error() != JSON_ERROR_NONE)){

			if($responses==null){
				$res_body = $data;
				$res_code = 400;
			} else if (array_key_exists($data, $responses)){
				$res_body = $data;
				$res_code = $responses[$data];
			} else {
				$res_body = array("Error"=>"Response not found");
				$res_code = 500;
			}
			echo json_encode($res_body);
			http_response_code($res_code);
		} else {
			echo $data;
		}
	}
}