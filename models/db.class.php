<?php

Class DB {	

    public $connection = null;
    public function __construct( $dbhost = SQL_HOST, $dbname = SQL_DATABASE, $username = SQL_USERNAME, $password    = SQL_PASSWORD){

        try{
            $this->connection = new PDO("mysql:host={$dbhost};dbname={$dbname};", $username, $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        }catch(Exception $e){
            throw new Exception($e->getMessage());   
        }			
    }
}