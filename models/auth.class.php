<?php 


Class Auth {

	private $db;
	private $headers;
	private $users;
	private $admin;
	private $contentType = null;
	public $authUser = null;

	public function __construct($db, $headers) {
		$this->db = $db;
		$this->headers = $headers;
		$this->users = new Users($db);

		if(isset($this->headers['Authorization'])){
			$authString = explode(" ",$this->headers['Authorization']);	
			
			if(isset($authString[1])){
				$this->authUser = $authString[1];
			} 	
		}
		if(isset($this->headers['Content-Type'])){
			$this->contentType = $this->headers['Content-Type'];
		}

		$dbUser = $this->users->getUserByName($this->authUser);
		
		if($dbUser && $dbUser['admin'] == 1){
			$this->admin = true;
		} else {
			$this->admin = false;
		}
	}

	public function checkAuthUser(){
		$dbUser = $this->users->getUserByName($this->authUser);
		if($dbUser){
			return true;
		}
		return false;
	}

	public function checkAdmin(){
		if($this->admin == true){
			return true;
		}
		return false;
	}

	public function checkContentType($contentType){
		if($contentType == $this->contentType){
			return true;
		}
		return false;
	}
}