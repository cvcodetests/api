  
<?php

header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization');
header('Content-Type: application/json');

require_once ('./includes/config.php');
require_once ('./includes/autoloader.php');

$db = new DB();
$users = new Users($db->connection);
$room = new Room($db->connection);

$allHeaders = getallheaders();
$auth = new Auth($db->connection, $allHeaders);

$requestMethod = $_SERVER['REQUEST_METHOD'];
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);
$uriCheck = ($uri[2] == "room" || $uri[2] == "usage") ;
$requestCheck = in_array($requestMethod, ["GET", "POST", "PUT", "DELETE", "OPTIONS"]);


if(!$auth->checkAuthUser()){
	http_response_code(401);
	die();
}

if ($uriCheck && $requestCheck) {

	switch ($requestMethod) {
		case 'GET':

			if($uri[2]== "room" || $uri[2]== "room/"){
				if(!isset($uri[3]) || $uri[3] == ""){
					//GET ROOM
					$rooms =  $room->getRoom($uri[3]);
				}else{ 
					///GET ROOMS
					$rooms =  $room->getRooms();
				} 
					$room->responseGenerator($rooms);
			} else if($uri[2] == "usage"){
				//GET USAGE
				if(isset($_REQUEST['startDate']) && isset($_REQUEST['endDate'])){
					if(!isset($_REQUEST['roomId'])){
						$_REQUEST['roomId'] = null;
					}
					$usage =  $room->getRoomUsage($_REQUEST['roomId'],$_REQUEST['startDate'],$_REQUEST['endDate']);
					$room->responseGenerator($usage);
				} else {
					//400 (Bad Request)
					$room->responseGenerator("Start and end date are required.", array("Start and end date are required."=>400));
				}
			}
			break;
		case 'POST':
			///POST ROOM
			if(isset($_REQUEST['id']) && isset($_REQUEST['name'])){
				if(!$auth->checkAdmin()){
					///401 (Unauthorized)
					$room->responseGenerator("Unauthorised to make this request", array("Unauthorised to make this request"=>401));
				}
				if(!$auth->checkContentType("application/json")){
					///415 (Unsupported Media Type)
					$room->responseGenerator("Invalid Content-Type", array("Invalid Content-Type"=>415));
				}
				$response = $room->postRoom($_REQUEST['id'],$_REQUEST['name']);
				//409 (Conflict)
				$room->responseGenerator($rooms, array("Room name or ID already in use."=>409));
			} else {
				//400 (Bad Request)
				$room->responseGenerator("Requires name and room ID", array("Requires name and room ID"=>400));
			}
			break;
				
		case 'PUT':
			if(isset($uri[3])){
				if(isset($_REQUEST['name'])){
					///PUT NAME
					//Conflict, No content (no body)
					$response = $room->putRoomName($uri[3],$_REQUEST['name']);
					$room->responseGenerator($response, array("Room name already in use."=>409, "Room not found."=>204));

				} else if (isset($_REQUEST['available'])) {
					///PUT AVAILABILITY
					if($_REQUEST['available'] == "true"){
						$user = false;
					} else if ($_REQUEST['available'] == "false") {
						$user = $auth->authUser;
					} else {
						$room->responseGenerator("Bad request.", array("Bad request."=>400));
					}

					$response = $room->putRoomUser($uri[3],$user);
					$room->responseGenerator($response);
				} else {
					$room->responseGenerator("Bad request.", array("Bad request."=>400));
				}
			}
			break;
	}
} else {
	$room->responseGenerator("Not found.", array("Not found."=>400));
}



